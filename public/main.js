const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");
const amount = 6;
const size = canvas.width / amount
const padding = 1;
const food_color = "#f44336";
const snake_color = "#388e3c";
const empty_color = "#424242";
const stroke_color = "#eeeeee";
const speed = 500;
const food_gen_time = 5;

const UP = "up";
const DOWN = "down";
const LEFT = "left";
const RIGHT = "right";

let lastmove;
let control;

let food_consumed = 0;
let snake;
let food;
let loop_counter;
let timer;

const point = (x, y) => {
  x = x < 0 ? amount - 1 : x % amount;
  y = y < 0 ? amount - 1 : y % amount;
  return {
    getX: () => x,
    setX: value => x = value,
    getY: () => y,
    setY: value => y = value,
    compare: o => o.getX() === x && o.getY() === y
  }
};

const getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
};

const isSnake = (point) => {
  return snake.some(s => s.compare(point));
}

const generateFood = () => {
  let f = point(getRandomInt(amount), getRandomInt(amount));
  if(!isSnake(f)) {
    food.push(f);
    drawFoodElement(f.getX(), f.getY());
  }
};

const loop = () => {
  moveSnake();
  if(loop_counter % food_gen_time == 0) {
    generateFood();
    loop_counter = 0;
  }1
  loop_counter++;
};

const start = () => {
  snake = [point(1,1), point(1,2), point(1,3)];
  food = [];
  food_consumed = 0;
  loop_counter = 0;
  lastmove = UP;
  control = UP;
  drawBoard();
  drawSnake();
  timer = setInterval(() => {
    loop();
  }, speed);
};

const pause = () => {
  clearInterval(timer);
  timer = null;
};

const moveSnake = () => {
  if(food_consumed > 0) {
    food_consumed--;
  } else {
    let se = snake.pop();
    drawEmptyElement(se.getX(), se.getY());
  }
  const head = snake[0];
  let next = point(head.getX(), head.getY() - 1);
  switch (control) {
    case DOWN:
      next = point(head.getX(), head.getY() + 1);
      break;
    case LEFT:
      next = point(head.getX() - 1, head.getY());
      break;
    case RIGHT:
      next = point(head.getX() + 1, head.getY());
      break;
  }
  if(isSnake(next)) {
    // Lost
    console.log("lost");
    pause();
    start();
    return;
  }
  snake.unshift(next);
  lastmove = control;
  drawSnakeElement(next.getX(), next.getY());
  if (food.some(f => f.compare(next))) {
    food = food.filter(f => f.getX() === next.getX() && f.getY() === next.getY());
    food_consumed++;
  }
};

const drawFoodElement = (x, y) => {
  drawCircle(x, y, food_color);
};

const drawSnakeElement = (x, y) => {
  drawCircle(x, y, snake_color);
};

const drawEmptyElement = (x, y) => {
  drawRect(x, y, empty_color);
};

const drawCircle = (x, y, color, border = 4) => {
  const radius = (size - 2 * border) / 2;
  ctx.fillStyle = color;
  ctx.strokeStyle = stroke_color;
  ctx.lineWidth = border / 4;
  ctx.beginPath();
  ctx.arc(x * size + radius + border, y * size + radius + border, radius, 0, 2 * Math.PI);
  ctx.closePath();
  ctx.stroke();
  ctx.fill();
};

const drawRect = (x, y, color, border = 3) => {
  ctx.fillStyle = color;
  ctx.fillRect(
    x * size + border,
    y * size + border,
    size - border * 2,
    size - border * 2);
};

const drawSnake = () => {
  snake.forEach(s => {
    drawSnakeElement(s.getX(), s.getY());
  });
};

const drawBoard = () => {
  ctx.clearRect(0, 0, ctx.width, ctx.height);
  for (let y = 0; y < amount; y++) {
    for (let x = 0; x < amount; x++) {
      drawEmptyElement(x , y);
    }
  }
};

const updateControl = (event) => {
  switch (event.key) {
    case "ArrowLeft":
      if(lastmove != RIGHT) {
        control = LEFT;
      }
      break;
    case "ArrowRight":
      if(lastmove != LEFT) {
        control = RIGHT;
      }
      break;
    case "ArrowUp":
      if(lastmove != DOWN) {
        control = UP;
      }
      break;
    case "ArrowDown":
      if(lastmove != UP) {
        control = DOWN;
      }
      break;
  }
};

document.onkeydown = updateControl;
start();